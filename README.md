## How to setup

1. Download repository: https://bitbucket.org/mczimm/parser/downloads
2. Unzip and cd to the unzipped directory with source 
3. Initiate virtualenv
4. Install the requirements into the virtualenv
5. Enjoy

````
cd unzipped_repository
sudo pip install virtualenv
virtualenv -p /usr/bin/python2.7 venv
source venv/bin/activate
pip install -r requirements.txt
````

---

## How to use

**Prepare a SQL file for the customer. Upload, and ask to execute. After that the customer should upload all generated CSV files.**

````
SET LINESIZE 20000
SET TRIMSPOOL ON
SET TRIMOUT ON
SET WRAP OFF
SET TERMOUT OFF
SET HEADING ON
SET COLSEP ,
SET HEADSEP OFF

spool param.csv
select name,value from V$PARAMETER where name in ('db_block_size', 'log_buffer');
/
spool off

spool sys_stat.csv
select name,value from v$sysstat where name like 'redo writes' or name like 'redo blocks written';
/
spool off

spool redo_view1.csv
select * from v$log;
/
spool off

spool redo_view2.csv
select * from v$logfile;
/
spool off
````

**Download all CSV files uploaded by customer to some work directory. And execute the parser script**

````
python parser.py /path/to/some_directory
````

**The script will generate result.html in the your work directory with all data from the CSV files**
