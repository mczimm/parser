#!/usr/bin/env python

import pandas
import re
import numpy
import sys
import glob


def rm_dash(csv_string):
    try:
        pattern = re.compile('-')
        if pattern.match(csv_string):
            return numpy.nan
        else:
            return csv_string
    except Exception, e:
        return e.message


def parser(argv):
    if argv:
        csv_cleared_df = pandas.DataFrame()
        with open(argv+'/result.html', 'w') as _file:
            for file_csv in glob.glob(argv+'/*.csv'):
                _a = -1
                csv_df = pandas.read_csv(file_csv, sep=',', error_bad_lines=False)

                for _key, _value in csv_df.iteritems():
                    _a = _a+1
                    if csv_cleared_df.empty:
                        csv_cleared_df = pandas.DataFrame(csv_df[_key].apply(rm_dash))
                    else:
                        csv_cleared_df.insert(_a, _key, pandas.DataFrame(csv_df[_key].apply(rm_dash)), allow_duplicates=True)
                    csv_cleared_df.dropna(inplace=True)
                _file.write(csv_cleared_df.to_html() + "\n\n")
                csv_cleared_df = csv_cleared_df.iloc[0:0]


if __name__ == '__main__':
    parser(sys.argv[1])
